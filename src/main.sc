require: localPatterns.sc
require: scripts/scripts.js
require: dicts/discounts.yaml
    var = discountInfo
require: city/cities-ru.csv
    module = sys.zb-common
    name = cities
    var = cities
require: slotfilling/slotFilling.sc
    module = sys.zb-common
require: scripts/integration.js


init:
    bind("postProcess", function($context){$context.session.lastState = $context.currentState});
    

theme: /
    
    state: bad
        intent!: /back_bot
        a: Слышь, умник! Сам лучше напиши!
    
    state: Hello
        intent!: /Привет
        a: Привет!
    
    
    state: Welcome
        q!: *start
        q: *start || fromState = /Phone/Ask, onlyThisState = true
        q!: $hi *
        script:
            $response.replies = $response.replies || [];
            $response.replies.push(getImage("Самолет", $injector.image));
        random:
            a: Здравствуйте!
            a: Приветствую!
        a: Меня зовут {{capitalize($injector.botName)}}
        go!: /Service/SuggestHelp
            
    state: CatchAll || noContext = true
        event!: noMatch
        a: Простите, я не понимаю вас. Переформулируйте свой вопрос.
        a: {{$session.lastState}}
        if: $session.lastState
            go!: {{$session.lastState}}
            
theme: /Service
    
    state: SuggestHelp
        q: * отмен* * || fromState = /Phone/Ask, onlyThisState = true
        a: Давайте я помогу вам купить билет на самолет, хорошо?
        buttons:
            "Да"
            "Нет"
        
        
        state: Accepted
            q: * $yes *
            a: Отлично!
            if: $client.phone
                go!: /Phone/Confirm
            else:
                go!: /Phone/Ask
            go!: /Phone/Ask
            
        state: Rejected
            q: * $no *
            a: Боюсь, что ничего другого я пока предложить не могу...
            
theme: /Phone
    
    state: Ask || modal = true
        a: Напишите ваш номер телефона, пожалуйста, в формате 79000000000.
        buttons:
            "Отмена"
        
        state: Get
            q: * $phone *
            a: Номер распознан
            go!: /Phone/Confirm
            
        state: Wrong
            q: *
            a: Что-то не похоже на номер телефона
            go!: ..
        
    state: Confirm
        script: $session.ProbablyPhone = $parseTree._phone || $client.phone;
        a: Ваш номер {{$session.ProbablyPhone}}, верно?
        buttons:
            "Да"
            "Нет"
            
        state: Yes
            q: * $yes *
            script:
                $client.phone = $session.ProbablyPhone || $client.phone;
                $response.client = $client;
                $analytics.setSessionData("Получен номер телефона", $client.phone);
                delete $session.ProbablyPhone;
            go!: /Discount/Inform
            
            
        state: No
            q: * $no *
            go!: /Phone/Ask
            
theme: /Discount
    
    state: Inform
        script:
            var nowDayWeek = $jsapi.dateForZone("Europe/Moscow", "EEEE");
            var discount = discountInfo[nowDayWeek];
            if (discount) {
                var nowDate = $jsapi.dateForZone("Europe/Moscow", "dd.MM");
                var answerText = "Сегодня, " + nowDate + ", действует акция";
                $reactions.answer(answerText);
                $reactions.answer(discount);
            }
        go!: /City/Departure

theme: /City
    
    state: Departure
        a: Назовите, пожалуйста, город отправления.
        
        state: Get
            # q: * $city *
            script:
                #log("!!!! city: " + toPrettyString($parseTree));
                $session.departureCity = $parseTree._city;
                $analytics.setSessionResult("Город определен правильно")
            a: Итак, город отправления: {{$session.departureCity.name}}
                
        state: localCatchAll
            event: noMatch
            a: Простите, я вас не понял
            go!: {{$session.lastState}}
        
    state: Direction
        intent!: /Ticket
        script: 
            log("!!! "+ toPrettyString($parseTree));
            $session.departureCity = $nlp.inflect($parseTree._Departure.name, "gent") || $parseTree._Departure.name;
            $session.destinationCity = $nlp.inflect($parseTree._Destination.name, "accs") || $parseTree._Destination.name;
            $session.departureCity = capitalize($session.departureCity);
            $session.destinationCity = capitalize($session.destinationCity);
            $session.time = $parseTree._Time.day + "/" + $parseTree._Time.month + "/" + $parseTree._Time.year
            $session.lat = $parseTree._Destination.lat;
            $session.lon = $parseTree._Destination.lon;
        a: Итак, {{$session.time}} отправляемся из {{$session.departureCity}} в {{$session.destinationCity}}.
        go!: /Weather/Current
        
theme: /Weather
    
    state: Current
        script:
            $temp.currentWeather = getCurrentWeather($session.lat,$session.lon);
        if: $temp.currentWeather
            a: В городе {{$session.destinationCity}} {{$temp.currentWeather.description}}, {{$temp.currentWeather.temp}} градусов Цельсия
    
        
    # state: Entity
    #     q!: * еду в *
    #     script: log("!!! сущность: " + toPrettyString($entities))
        
    # state: Entity pattern
    #     q!: * мне нужен @Agent::ag
    #     a: Не могу позвать {{$parseTree._ag}}.
    