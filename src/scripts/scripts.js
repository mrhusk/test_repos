function getImage(description, imageUrl){
    return{
        "type": "image",
        "imageUrl": imageUrl,
        "text": description
    };
}