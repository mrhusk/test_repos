function getCurrentWeather(lat, lon) {
    var apiKey = $jsapi.context().injector.apiKey;
    var response = $http.get("http://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${lon}&appid=${APIkey}&units=${units}&lang=${lang}", {
        query: {
            lat: lat,
            lon: lon,
            APIkey: apiKey,
            units: "metric",
            lang: "ru"
        }
    })
    if (!response.isOk || !response.data){
        return false;
    }
    log(toPrettyString(response));
    var weather = {};
    weather.temp = response.data.main.temp;
    weather.description = response.data.weather[0].description;
    return weather;
}