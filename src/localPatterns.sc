patterns:
    $hi = (привет*/здравствуй*)
    $phone = $regexp<79\d{9}>
    $yes = * (да/давай*/хорошо) *
    $no = * (нет/не [надо/нужно]) *
    $city = $entity<cities> || converter = function(parseTree) {var id = parseTree.cities[0].value; return cities[id].value}